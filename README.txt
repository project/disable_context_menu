CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer:
  Alexander Müller (http://drupal.org/user/641556)
  Yannick Leyendecker (http://drupal.org/user/531118)

This module enables you to bother users by deny the use
of the context menu on certain elements.


INSTALLATION
------------

1. Install the module the drupal way [1]

2. Go to configuration page and enter the selectors.
   Optional you can enter a block message.


[1] http://drupal.org/documentation/install/modules-themes/modules-7
